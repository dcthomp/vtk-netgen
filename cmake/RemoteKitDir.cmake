function(remote_kit_dir kitvar)
  string(REPLACE "${${PROJECT_NAME}_SOURCE_DIR}/" "" kit ${CMAKE_CURRENT_SOURCE_DIR})
  # string(REPLACE "/" "_" kit "${dir_prefix}")
  set(${kitvar} "${kit}" PARENT_SCOPE)
  # Optional second argument to get dir_prefix.
  if (${ARGC} GREATER 1)
    set(${ARGV1} "${dir_prefix}" PARENT_SCOPE)
  endif (${ARGC} GREATER 1)
endfunction(remote_kit_dir)
